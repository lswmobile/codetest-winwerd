//
//  TaggedText.swift
//  CodeTest
//
//  Created by Popeye on 6/2/2558 BE.
//  Copyright (c) 2558 lsw. All rights reserved.
//

import Foundation

class TaggedText: NSObject {
    var text: String = ""
    var textMappings:NSArray = []
}