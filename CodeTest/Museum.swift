//
//  Museum.swift
//  CodeTest
//
//  Created by Popeye on 6/2/2558 BE.
//  Copyright (c) 2558 lsw. All rights reserved.
//

import Foundation

class Museum: NSObject {
    dynamic var name: String = ""
    dynamic var photo: String = ""
    dynamic var location: String = ""
}