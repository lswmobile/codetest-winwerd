//
//  InfoTableViewCell.swift
//  CodeTest
//
//  Created by Popeye on 6/2/2558 BE.
//  Copyright (c) 2558 lsw. All rights reserved.
//

import Foundation
import UIKit
import TTTAttributedLabel
import SnapKit
import SDWebImage

class InfoTableViewCell: UITableViewCell, TTTAttributedLabelDelegate {
    var nameLabel: TTTAttributedLabel!
    var locationLabel: TTTAttributedLabel!
    var museumImage: UIImageView!
    var filteredView: UIView!

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        // setup name label
        self.nameLabel = TTTAttributedLabel()
        self.nameLabel.font = UIFont(name: kUIConstant.kFont.kDefault, size: kUIConstant.kTable.kCell.kLabel.fontSize)
        self.nameLabel.textColor = UIColor.whiteColor()
        self.nameLabel.numberOfLines = 0
        self.nameLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.nameLabel.textAlignment = NSTextAlignment.Center
        self.nameLabel.kern = kUIConstant.kTable.kCell.kLabel.kern

        // setup image
        self.museumImage = UIImageView(frame: CGRectMake(0, 0, self.frame.size.width, kUIConstant.kTable.kCell.kMuseum.height))
        self.museumImage.autoresizingMask = UIViewAutoresizing.FlexibleWidth

        self.filteredView = UIView(frame: self.museumImage.frame)
        self.filteredView.autoresizingMask = UIViewAutoresizing.FlexibleWidth
        self.filteredView.opaque = true
        self.filteredView.alpha = kUIConstant.kTable.kCell.kMuseum.kFilterView.alpha
        self.museumImage.addSubview(self.filteredView)
        self.museumImage.bringSubviewToFront(self.filteredView)

        // setup location label
        self.locationLabel = TTTAttributedLabel()
        self.locationLabel.font = UIFont(name: kUIConstant.kFont.kDefault, size: kUIConstant.kTable.kCell.kMuseum.kLocationLabel.fontSize)
        self.locationLabel.textColor = UIColor.whiteColor()
        self.locationLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.locationLabel.textAlignment = NSTextAlignment.Center

        // handle subviews
        self.museumImage.addSubview(self.nameLabel)
        self.museumImage.addSubview(self.locationLabel)
        self.contentView.addSubview(self.museumImage)

        self.museumImage.bringSubviewToFront(self.nameLabel)
        self.museumImage.bringSubviewToFront(self.locationLabel)

        self.nameLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.locationLabel.setTranslatesAutoresizingMaskIntoConstraints(false)

        self.nameLabel.snp_makeConstraints { (make) -> Void in
            make.center.equalTo(self.snp_center)
            make.width.equalTo(self.museumImage.snp_width).offset(-kUIConstant.kTable.kCell.kMuseum.kNameLabel.margin)
        }

        self.locationLabel.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(kUIConstant.kTable.kCell.kMuseum.kLocationLabel.margin.left)
            make.bottom.equalTo(-kUIConstant.kTable.kCell.kMuseum.kLocationLabel.margin.bottom)
        }
    }

    func applyFilter(index: Int) {

        if index % kUIConstant.kTable.kCell.kMuseum.filtersNumber == 0 {
            self.filteredView.backgroundColor = UIColor.blackColor()
        }
        else if index % kUIConstant.kTable.kCell.kMuseum.filtersNumber == 1 {
            self.filteredView.backgroundColor = kUIConstant.kColors.lightBlue
        }
        else if index % kUIConstant.kTable.kCell.kMuseum.filtersNumber == 2 {
            self.filteredView.backgroundColor = UIColor.redColor()
        }

    }
}
