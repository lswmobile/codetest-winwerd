//
//  ExhibitsTableViewCell.swift
//  CodeTest
//
//  Created by Popeye on 6/4/2558 BE.
//  Copyright (c) 2558 lsw. All rights reserved.
//

import Foundation
import UIKit
import TTTAttributedLabel
import SnapKit
import SDWebImage

class ExhibitsTableViewCell: UITableViewCell, TTTAttributedLabelDelegate {
    var nameLabel: TTTAttributedLabel!
    var curatorHeaderLabel: TTTAttributedLabel!
    var taggedTextLabel: TTTAttributedLabel!
    var photo: UIImageView!
    var curatorPhoto: UIImageView!

    var accessoryButton: UIButton!

    var exhibits: Exhibits?

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        // setup name label
        self.nameLabel = TTTAttributedLabel()
        self.nameLabel.font = UIFont(name: kUIConstant.kFont.kDefault, size: kUIConstant.kTable.kCell.kExhibits.kNameLabel.fontSize)
        self.nameLabel.numberOfLines = 0
        self.nameLabel.textColor = UIColor.whiteColor()
        self.nameLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.nameLabel.textAlignment = NSTextAlignment.Left
        self.nameLabel.autoresizingMask = UIViewAutoresizing.FlexibleWidth
        self.nameLabel.kern = kUIConstant.kTable.kCell.kExhibits.kNameLabel.kern

        // setup image
        self.photo = UIImageView()
        self.photo.autoresizingMask = UIViewAutoresizing.FlexibleWidth

        self.accessoryButton = UIButton()
        self.accessoryButton.setImage(UIImage(named: "accessory"), forState: UIControlState.Normal)
        self.accessoryButton.frame = kUIConstant.kTable.kCell.kExhibits.kAccessoryButton.frame
        self.accessoryButton.userInteractionEnabled = true

        self.photo.addSubview(accessoryButton)
        self.photo.userInteractionEnabled = true

        self.curatorPhoto = UIImageView()
        self.curatorPhoto.contentMode = UIViewContentMode.ScaleAspectFit

        // setup curatorLabel
        self.curatorHeaderLabel = TTTAttributedLabel()
        self.curatorHeaderLabel.font = UIFont(name: kUIConstant.kFont.kDefault, size: kUIConstant.kTable.kCell.kExhibits.kCuratorHeaderLabel.fontSize)
        self.curatorHeaderLabel.delegate = self
        self.curatorHeaderLabel.textColor = UIColor.blackColor()
        self.curatorHeaderLabel.numberOfLines = 0

        // setup taggedTextLabel label
        self.taggedTextLabel = TTTAttributedLabel()
        self.taggedTextLabel.numberOfLines = 0
        self.taggedTextLabel.font = UIFont(name: kUIConstant.kFont.kDefault, size: kUIConstant.kTable.kCell.kExhibits.kTaggedTextLabel.fontSize)
        self.taggedTextLabel.textColor = kUIConstant.kColors.greyText
        self.taggedTextLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.taggedTextLabel.delegate = self
        self.taggedTextLabel.linkAttributes = [kCTForegroundColorAttributeName : kUIConstant.kColors.darkBlueText, NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        self.taggedTextLabel.activeLinkAttributes = [kCTForegroundColorAttributeName: UIColor.blueColor(), NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        self.taggedTextLabel.enabledTextCheckingTypes = NSTextCheckingType.Link.rawValue

        // handle subviews
        self.photo.addSubview(self.nameLabel)
        self.contentView.addSubview(self.curatorHeaderLabel)
        self.contentView.addSubview(self.taggedTextLabel)
        self.contentView.addSubview(self.photo)
        self.contentView.addSubview(self.curatorPhoto)

        self.contentView.bringSubviewToFront(self.nameLabel)
        self.contentView.bringSubviewToFront(self.taggedTextLabel)
    }

    func setExhibitsObject(exhibits: Exhibits) {
        self.exhibits = exhibits

        self.nameLabel.setText(exhibits.name)
        self.photo.sd_setImageWithURL(NSURL(string: exhibits.photo))
        self.taggedTextLabel.setText(exhibits.taggedText.text)
        self.curatorPhoto.sd_setImageWithURL(NSURL(string: exhibits.curatorPhoto))
        self.curatorHeaderLabel.setText(kUIConstant.kTable.kCell.kExhibits.kCuratorHeaderLabel.text)

        self.adjustLayout()
    }

    // This is for mock up the height of label when the cell invisible
    // Make the app be able to calculated the height of each cell properly
    class func getNameLabelAttributedString(text: String) -> NSAttributedString {
        var nameLabel = TTTAttributedLabel()
        nameLabel.font = UIFont(name: kUIConstant.kFont.kDefault, size: kUIConstant.kTable.kCell.kExhibits.kNameLabel.fontSize)
        nameLabel.numberOfLines = 0
        nameLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
        nameLabel.textAlignment = NSTextAlignment.Center
        nameLabel.verticalAlignment = TTTAttributedLabelVerticalAlignment.Center
        nameLabel.autoresizingMask = UIViewAutoresizing.FlexibleWidth
        nameLabel.text = text

        return nameLabel.attributedText
    }

    class func getTaggedTextLabelAttributedString(text: String) -> NSAttributedString {
        var taggedTextLabel = TTTAttributedLabel()
        taggedTextLabel.firstLineIndent = kUIConstant.kTable.kCell.kLabel.indent
        taggedTextLabel.numberOfLines = 0
        taggedTextLabel.font = UIFont(name: kUIConstant.kFont.kDefault, size: kUIConstant.kTable.kCell.kExhibits.kTaggedTextLabel.fontSize)
        taggedTextLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
        taggedTextLabel.text = text
        
        return taggedTextLabel.attributedText
    }

    class func getCuratorHeaderLabelAttributedString() -> NSAttributedString {
        var curatorHeaderLabel = TTTAttributedLabel()
        curatorHeaderLabel.font = UIFont(name: kUIConstant.kFont.kDefault, size: kUIConstant.kTable.kCell.kExhibits.kCuratorHeaderLabel.fontSize)
        curatorHeaderLabel.numberOfLines = 0
        curatorHeaderLabel.text = kUIConstant.kTable.kCell.kExhibits.kCuratorHeaderLabel.text

        return curatorHeaderLabel.attributedText
    }

    // MARK- TTTAttributedTextDelegate
    func attributedLabel(label: TTTAttributedLabel!, didSelectLinkWithPhoneNumber phoneNumber: String!) {
        println("From click link --> \(phoneNumber)")
    }

    // MARK- Private methods
    private func searchMapText(searchString: String) -> (value: String, type: String)? {
        if self.exhibits != nil && self.exhibits!.taggedText.textMappings.count > 0 {
            var textMapping = self.exhibits!.taggedText.textMappings.bk_select({ (each:AnyObject!) -> Bool in
                let dict = each as! NSDictionary
                let key: String = dict.allKeys[0] as! String
                return key == searchString
            })

            if textMapping.count > 0 {
                let dict = textMapping[0] as! NSDictionary
                let key: String = dict.allKeys[0] as! String // populate the first key; there is only 1 key here
                let mappedText: AnyObject? = dict.objectForKey(key)
                return (mappedText!.objectForKey("value") as! String, mappedText!.objectForKey("type") as! String)
            }
        }
        return nil
    }

    private func adjustLayout() {

        dispatch_async(dispatch_get_main_queue()) {

            /* Adjust the links to taggedText */
            if self.exhibits != nil && self.exhibits!.taggedText.textMappings.count > 0 {
                // We capture link by search ${ } before we replace them
                let linkArrays = self.searchRangeOfString(kGlobalConstant.beginLinkString, endString: kGlobalConstant.endLinkString, inString: self.taggedTextLabel.text!)

                // Replace out "${" and "}"
                var replacedText = (self.exhibits!.taggedText.text as NSString).stringByReplacingOccurrencesOfString(kGlobalConstant.beginLinkString, withString: "")
                replacedText = (replacedText as NSString).stringByReplacingOccurrencesOfString(kGlobalConstant.endLinkString, withString: "")
                self.taggedTextLabel.setText(replacedText)

                var extraShiftLenght = 0
                var linkedValues:[(value: String, type: String)?] = []

                // Replace string and adjust the link range here
                for var i = 0; i < linkArrays.count; i++ {
                    if let range = linkArrays.objectAtIndex(i) as? NSRange {
                        // for the second link further we need to adjust the range of link text because we just replace out ${}
                        var adjustedRange = range

                        adjustedRange = NSMakeRange(range.location + extraShiftLenght - ((count(kGlobalConstant.beginLinkString) + count(kGlobalConstant.endLinkString)) * i), range.length - (count(kGlobalConstant.beginLinkString) + count(kGlobalConstant.endLinkString)))

                        let targetText = (self.taggedTextLabel.text! as NSString).substringWithRange(adjustedRange)
                        let linkValue = self.searchMapText(targetText)

                        // if we found link we replace text and adjust range link first
                        if linkValue != nil {

                            // Replace the text with the link value
                            let replacedLinkText = (self.taggedTextLabel.text! as NSString).stringByReplacingCharactersInRange(adjustedRange, withString: linkValue!.value)
                            self.taggedTextLabel.setText(replacedLinkText)

                            // We check if the real string will change the order of others links, we have to adjust range according to it; example ${1800} -> 1800s
                            if count(linkValue!.value) != count(targetText) {
                                adjustedRange = NSMakeRange(adjustedRange.location, count(linkValue!.value))
                                extraShiftLenght += abs(count(linkValue!.value) - count(targetText))
                            }

                            // store for use in the next stage; add link to the paragraph
                            linkedValues += [linkValue]
                        }
                        else {
                            // if it not found link we just push the empty array
                            linkedValues += [nil]
                        }

                        // Re-Setup the range of each link after replaced
                        linkArrays.replaceObjectAtIndex(i, withObject: adjustedRange)
                    }
                }

                // Add link to the paragraph
                for var i = 0; i < linkArrays.count; i++ {
                    if let range = linkArrays.objectAtIndex(i) as? NSRange {

                        let targetText = (self.taggedTextLabel.text! as NSString).substringWithRange(range)
                        let linkValue = linkedValues[i]

                        // if we found link we just add it
                        if linkValue != nil && linkValue!.type == "art_object" {
                            let urlContent: String = "VALUE: \(linkValue!.value), TYPE: \(linkValue!.type)"
                            // I choose PhoneNumber over URL because some string cannot make NSURL
                            self.taggedTextLabel.addLinkToPhoneNumber(urlContent, withRange: range)
                        }
                    }
                }
            }

            // Layout all subviews
            /* photo */
            var height = TTTAttributedLabel.sizeThatFitsAttributedString(self.nameLabel.attributedText, withConstraints: CGSizeMake(self.frame.width, CGFloat.max), limitedToNumberOfLines: 0).height
            var width = self.frame.size.width - kUIConstant.kTable.kCell.minorMargin.left - kUIConstant.kTable.kCell.minorMargin.right

            self.photo.frame = CGRectMake(0, 0, self.frame.size.width, kUIConstant.kTable.kCell.kImage.height)

            /* nameLabel */
            var x = kUIConstant.kTable.kCell.kExhibits.kNameLabel.margin
            var y = self.photo.frame.size.height - height - kUIConstant.kTable.kCell.minorMargin.bottom
            self.nameLabel.frame = CGRectMake(x, y, width, height)

            /* accessoryButton */
            x = self.photo.frame.size.width - self.accessoryButton.frame.size.width - kUIConstant.kTable.kCell.kExhibits.kAccessoryButton.margin
            y = (self.photo.frame.size.height / 2) - (self.accessoryButton.frame.size.height / 2)
            self.accessoryButton.frame = CGRectMake(x , y, self.accessoryButton.frame.size.width, self.accessoryButton.frame.size.height)

            /* curatorHeaderLabel */
            width = self.frame.size.width - kUIConstant.kTable.kCell.kExhibits.kCuratorPhoto.width - (kUIConstant.kTable.kCell.kExhibits.kTaggedTextLabel.margin.left + kUIConstant.kTable.kCell.kExhibits.kTaggedTextLabel.margin.right)
            height = TTTAttributedLabel.sizeThatFitsAttributedString(self.curatorHeaderLabel.attributedText, withConstraints: CGSizeMake(width, CGFloat.max), limitedToNumberOfLines: 0).height
            x = kUIConstant.kTable.kCell.kExhibits.kTaggedTextLabel.margin.left
            y = self.photo.frame.origin.y + self.photo.frame.height + kUIConstant.kTable.kCell.kExhibits.kCuratorHeaderLabel.margin.top
            self.curatorHeaderLabel.frame = CGRectMake(x, y, width, height)

            /* taggedTextLabel */
            height = TTTAttributedLabel.sizeThatFitsAttributedString(self.taggedTextLabel.attributedText, withConstraints: CGSizeMake(width, CGFloat.max), limitedToNumberOfLines: 0).height
            y = self.curatorHeaderLabel.frame.origin.y + self.curatorHeaderLabel.frame.size.height + kUIConstant.kTable.kCell.kExhibits.kCuratorHeaderLabel.margin.bottom
            self.taggedTextLabel.frame = CGRectMake(x, y, width, height)

            /* curatorPhoto */
            width = kUIConstant.kTable.kCell.kExhibits.kCuratorPhoto.width
            x = self.taggedTextLabel.frame.size.width + kUIConstant.kTable.kCell.kExhibits.kCuratorPhoto.margin + kUIConstant.kTable.kCell.kExhibits.kTaggedTextLabel.margin.left
            // if the text height is more than curator photo
            if self.taggedTextLabel.frame.height + self.curatorHeaderLabel.frame.height + kUIConstant.kTable.kCell.kExhibits.kCuratorHeaderLabel.margin.bottom + kUIConstant.kTable.kCell.kExhibits.kCuratorHeaderLabel.margin.top < kUIConstant.kTable.kCell.kImage.kCurator.height {
                y = self.photo.frame.origin.y + self.photo.frame.height
            }
            // if the text height is less than curator photo
            else {
                y = self.taggedTextLabel.frame.height + self.curatorHeaderLabel.frame.height + kUIConstant.kTable.kCell.kExhibits.kCuratorHeaderLabel.margin.bottom + kUIConstant.kTable.kCell.kExhibits.kCuratorHeaderLabel.margin.top + kUIConstant.kTable.kCell.kExhibits.kCuratorHeaderLabel.margin.bottom
            }

            self.curatorPhoto.frame = CGRectMake(x, y, width, kUIConstant.kTable.kCell.kImage.kCurator.height)
        }
    }

    private func searchRangeOfString(beginString: NSString, endString: NSString, inString: NSString) -> NSMutableArray {
        var results: NSMutableArray = NSMutableArray()
        var searchRange: NSRange = NSMakeRange(0, inString.length)
        var beginRange: NSRange, endRange: NSRange
        do {
            beginRange = inString.rangeOfString(beginString as String, options: NSStringCompareOptions.allZeros, range: searchRange)
            endRange = inString.rangeOfString(endString as String, options: NSStringCompareOptions.allZeros, range: searchRange)

            if beginRange.location != NSNotFound && endRange.location != NSNotFound && beginRange.location < inString.length && endRange.location < inString.length {
                let beginIndex = beginRange.location
                let length = endRange.location + endRange.length - beginRange.location
                var resultRange:NSRange = NSMakeRange(beginIndex, length)
                results.addObject(NSValue(range: resultRange))
                searchRange = NSMakeRange(NSMaxRange(endRange), inString.length - NSMaxRange(endRange))
            }
        }
            while beginRange.location != NSNotFound && endRange.location != NSNotFound && searchRange.location < inString.length
        return results
    }
}
