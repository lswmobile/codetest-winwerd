//
//  ExhibitParser.swift
//  CodeTest
//
//  Created by Popeye on 6/3/2558 BE.
//  Copyright (c) 2558 lsw. All rights reserved.
//

import Foundation

class ExhibitsParser: NSObject {

    class func parseObject(object: AnyObject) -> Exhibits {

        var parseObject: Exhibits = Exhibits()

        // we start at 1 because we want to skip super attribute
        for var index = 1; index < reflect(parseObject).count; ++index {

            let key = reflect(parseObject)[index].0

            // it will assign the value only if it's not null
            if let value = object.objectForKey(key) as? String {
                parseObject.setValue(value, forKey: key)
            }
            // This one is for nested object
            if let value = object.objectForKey(key) as? NSDictionary {
                var taggedText = TaggedTextParser.parseObject(value)
                parseObject.setValue(taggedText, forKey: key)
            }
        }

        return parseObject
    }
}