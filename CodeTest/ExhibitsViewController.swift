//
//  ExhibitsViewController.swift
//  CodeTest
//
//  Created by Popeye on 6/3/2558 BE.
//  Copyright (c) 2558 lsw. All rights reserved.
//

import UIKit
import SwiftHTTP
import TTTAttributedLabel
import ReactiveCocoa

class ExhibitsViewController: UITableViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating {

    var request:HTTPTask?
    var exhibits: NSMutableArray?

    var filterExhibits: NSMutableArray?
    var searchController: UISearchController?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Exhibits"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: kUIConstant.kFont.kDefault, size: kUIConstant.kNavigationBar.fontSize)!]
        self.navigationController?.navigationBar.barTintColor = kUIConstant.kColors.navBar
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.view.backgroundColor = UIColor.lightGrayColor()

        // setup navbar buttons
        var searchButton: UIButton = UIButton()
        searchButton.setImage(UIImage(named: "search"), forState: UIControlState.Normal)
        searchButton.frame = kUIConstant.kNavigationBar.navbarButtonFrame
        searchButton.addTarget(self, action: "searchPerformed", forControlEvents: UIControlEvents.TouchUpInside)

        var menuButton: UIButton = UIButton()
        menuButton.setImage(UIImage(named: "bars"), forState: UIControlState.Normal)
        menuButton.frame = kUIConstant.kNavigationBar.navbarButtonFrame
        menuButton.addTarget(self, action: "backPerformed", forControlEvents: UIControlEvents.TouchUpInside)

        let rightItem: UIBarButtonItem = UIBarButtonItem()
        rightItem.customView = searchButton
        self.navigationItem.rightBarButtonItem = rightItem

        let leftItem: UIBarButtonItem = UIBarButtonItem()
        leftItem.customView = menuButton
        self.navigationItem.leftBarButtonItem = leftItem

        // Hide back button
        self.navigationItem.hidesBackButton = true

        // Setup Search Display Controller
        self.searchController = UISearchController(searchResultsController: nil) // pass nil for showing the result in the same page
        self.searchController!.delegate = self
        self.searchController!.searchResultsUpdater = self
        self.searchController!.searchBar.delegate = self
        self.searchController!.dimsBackgroundDuringPresentation = false

        // Setup Search Bar
        self.searchController!.searchBar.frame = CGRectMake(0, 0, self.view.frame.width, kUIConstant.kSearchBar.height)
        self.searchController!.searchBar.delegate = self
        self.searchController!.searchBar.placeholder = "Search for Exhibits"
        self.searchController!.searchBar.searchBarStyle = UISearchBarStyle.Default
        self.searchController!.searchBar.translucent = false
        self.searchController!.searchBar.sizeToFit()

        // hide search bar
        self.tableView.setContentOffset(CGPointMake(0, 44), animated: true)

        // Attatch search to tableView
        self.tableView.tableHeaderView = self.searchController?.searchBar
        self.view.backgroundColor = UIColor.whiteColor()

        // Initialise exhibits array
        self.exhibits = []
        self.filterExhibits = []

        // Fetch exhibits to the list
        self.requestExhibits()

        // setup dynamic row height
        self.tableView.estimatedRowHeight = kUIConstant.kTable.kCell.kExhibits.height
        self.tableView.rowHeight = UITableViewAutomaticDimension

        self.tableView.separatorInset = UIEdgeInsetsZero
        self.tableView.layoutMargins = UIEdgeInsetsZero
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None

        // hide status bar when search
        self.definesPresentationContext = true
        self.view.backgroundColor = UIColor.whiteColor()

        // Add gesture recognition
        let recognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "backPerformed")
        recognizer.direction = UISwipeGestureRecognizerDirection.Left
        self.tableView.addGestureRecognizer(recognizer)
    }

    // MARK: - Send request functions
    func requestExhibits() {
        // Send GET request to populate the musium
        self.request = HTTPTask()
        //The expected response will be JSON and be converted to an object return by NSJSONSerialization instead of a NSData.
        self.request?.responseSerializer = JSONResponseSerializer()

        self.request?.GET(kRequestURL.kExhibits, parameters: nil, completionHandler: { (response: HTTPResponse) -> Void in
            println("Response from \(response.URL!) has arrived with status \(response.statusCode).")
            if let err = response.error {
                println("error: \(err.localizedDescription)")
                return // we may need to nitify app for failure; show alert for example
            }

            if let array:NSArray = response.responseObject!.objectForKey(kRequestURL.kDataKey) as? NSArray {
                println("Parsing Exhibits object...")
                array.bk_each({ (each: AnyObject!) -> Void in
                    let exhibits = ExhibitsParser.parseObject(each)
                    self.exhibits?.addObject(exhibits)

                    println("\t- \(exhibits.name)")
                })
                println("pairsed \(self.exhibits!.count) object(s)")

                dispatch_async(dispatch_get_main_queue()) {
                    let transition = CATransition()
                    transition.type = kCATransitionFromTop
                    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                    transition.fillMode = kCAFillModeForwards
                    transition.duration = kUIConstant.kTable.kCell.animationDuration
                    transition.subtype = kCATransitionFromTop
                    self.tableView.layer.addAnimation(transition, forKey: "UITableViewReloadDataAnimationKey")
                    self.tableView.reloadData()
                }
            }
            else {
                println("There are no data sending back")
            }
        })
    }

    // MARK - UITableViewDataSource implementation
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.exhibits == nil {
            return 0
        }
        if self.searchController!.active {
            return self.filterExhibits!.count
        }
        return self.exhibits!.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifierString = "ExhibitsTableViewCell"

        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier(identifierString) as? UITableViewCell

        if cell == nil {

            cell = ExhibitsTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: identifierString)
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
        }

        var exhibits: Exhibits?

        if self.searchController!.active {
            exhibits = self.filterExhibits?.objectAtIndex(indexPath.row) as? Exhibits
        }
        else {
            exhibits = self.exhibits?.objectAtIndex(indexPath.row) as? Exhibits
        }

        // inject data to cell's UI
        var exhibitsTableViewCell = (cell as! ExhibitsTableViewCell)
        if exhibits != nil {

            exhibitsTableViewCell.setExhibitsObject(exhibits!)
            // link the accessoryButton with the back function
            exhibitsTableViewCell.accessoryButton.addTarget(self, action: "backPerformed", forControlEvents: UIControlEvents.TouchUpInside)
        }

        cell?.layoutMargins = UIEdgeInsetsZero

        return cell!
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

        if let exhibits: Exhibits? = self.exhibits?.objectAtIndex(indexPath.row) as? Exhibits {
            let width = self.tableView.frame.width - kUIConstant.kTable.kCell.kExhibits.kCuratorPhoto.width - (kUIConstant.kTable.kCell.kExhibits.kTaggedTextLabel.margin.left + kUIConstant.kTable.kCell.kExhibits.kTaggedTextLabel.margin.right)

            let taggedTextHeight = TTTAttributedLabel.sizeThatFitsAttributedString(ExhibitsTableViewCell.getTaggedTextLabelAttributedString(exhibits!.taggedText.text), withConstraints: CGSizeMake(width, CGFloat.max), limitedToNumberOfLines: 0).height

            let curatorHeaderLabelHeight = TTTAttributedLabel.sizeThatFitsAttributedString(ExhibitsTableViewCell.getCuratorHeaderLabelAttributedString(), withConstraints: CGSizeMake(width, CGFloat.max), limitedToNumberOfLines: 0).height + kUIConstant.kTable.kCell.kExhibits.kCuratorHeaderLabel.margin.bottom + kUIConstant.kTable.kCell.kExhibits.kCuratorHeaderLabel.margin.top

            let textContentHeight = taggedTextHeight + curatorHeaderLabelHeight

            let photoHeight = kUIConstant.kTable.kCell.kImage.height

            var resultHeight = kUIConstant.kTable.kCell.margin.bottom
            if textContentHeight < kUIConstant.kTable.kCell.kImage.kCurator.height {
                resultHeight += photoHeight + kUIConstant.kTable.kCell.kImage.kCurator.height
            }
            else {
                resultHeight += photoHeight + textContentHeight
            }

            return resultHeight
        }

        return kUIConstant.kTable.kCell.kExhibits.height
    }

    // MARK: - Search
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        println("current search text \(searchText)")

        self.search(searchText)
    }

    func search(searchText: String) {

        let predicate = NSPredicate { (obj: AnyObject!, dic: [NSObject : AnyObject]!) -> Bool in
            if searchText.isEmpty {
                return true;
            }
            let exhibits = (obj as! Exhibits)
            return exhibits.name.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil, locale: nil) != nil
        }
        // filter the exhibits list
        self.filterExhibits = NSMutableArray(array: self.exhibits!.filteredArrayUsingPredicate(predicate))
        dispatch_async(dispatch_get_main_queue()) {
            let transition = CATransition()
            transition.type = kCATransitionFade
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.fillMode = kCAFillModeForwards
            transition.duration = kUIConstant.kTable.kCell.animationDuration
            transition.subtype = kCATransitionFromTop
            self.tableView.layer.addAnimation(transition, forKey: "UITableViewReloadDataAnimationKey")
            self.tableView.reloadData()
        }
    }

    // MARK: - UISearchResultsUpdating
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        self.filterExhibits?.removeAllObjects()

        self.search(self.searchController!.searchBar.text)
    }

    func searchPerformed() {

        if self.tableView.contentOffset.y < 44 {
            self.tableView.setContentOffset(CGPointMake(0, 44), animated: true)
        }
        else {
            self.tableView.setContentOffset(CGPointMake(0, 0), animated: true)
        }
    }

    // MARK: - Delegate Function
    func backPerformed() {
        println("backPerformed()")
        self.navigationController?.popViewControllerAnimated(true)
    }
}
