//
//  kUIConstant.swift
//  CodeTest
//
//  Created by Popeye on 6/2/2558 BE.
//  Copyright (c) 2558 lsw. All rights reserved.
//

import Foundation
import UIColor_Hex_Swift

struct kUIConstant {

    struct kNavigationBar {
        static let height:CGFloat = 44
        static let fontSize: CGFloat = 18
        static let navbarButtonFrame: CGRect = CGRectMake(0, 0, 15, 15)
    }

    struct kSearchBar {
        static let height:CGFloat = 44
    }

    struct kTable {
        struct kCell {
            static let margin: UIEdgeInsets = UIEdgeInsetsMake(20, 20, 20, 20)
            static let minorMargin: UIEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
            static let animationDuration: CFTimeInterval = 0.5

            struct kLabel {
                static let height: CGFloat = 44
                static let fontSize: CGFloat = 20
                static let indent: CGFloat = 20
                static let kern: CGFloat = 1
            }

            struct kImage {
                static let height: CGFloat = 145

                struct kCurator {
                    static let height: CGFloat = 140
                }
            }

            struct kMuseum {
                static let height: CGFloat = 145
                static let filtersNumber: Int = 3

                struct kNameLabel {
                    static let height: CGFloat = 44
                    static let margin: CGFloat = 35
                }

                struct kLocationLabel {
                    static let fontSize: CGFloat = 12
                    static let margin: UIEdgeInsets = UIEdgeInsetsMake(0, 10, 5, 0)
                }

                struct kFilterView {
                    static let alpha: CGFloat = 0.3
                }
            }

            struct kExhibits {
                static let height: CGFloat = 400

                struct kNameLabel {
                    static let margin: CGFloat = 20
                    static let fontSize: CGFloat = 18
                    static let kern: CGFloat = -1
                }

                struct kCuratorPhoto {
                    static let width: CGFloat = 135
                    static let margin: CGFloat = 20
                }

                struct kAccessoryButton {
                    static let frame: CGRect = CGRectMake(0, 0, 15, 15)
                    static let margin: CGFloat = 10
                }

                struct kCuratorHeaderLabel {
                    static let fontSize: CGFloat = 10
                    static let margin: UIEdgeInsets = UIEdgeInsetsMake(20, 20, 5, 0)
                    static let text: String = "Note from the curator"
                }

                struct kTaggedTextLabel {
                    static let fontSize: CGFloat = 10
                    static let margin: UIEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 30)
                }
            }
        }
    }

    struct kFont {
        static let kDefault:String = "LucidaGrande"
        static let kBold: String = "LucidaGrande-Bold"
    }

    struct kColors {
        static let darkBlueText: UIColor = UIColor.colorWithCSS("#5A8097")
        static let greyText: UIColor = UIColor.colorWithCSS("#808080")
        static let navBar: UIColor = UIColor.colorWithCSS("#496D85")

        static let lightBlue: UIColor = UIColor.colorWithCSS("#39EFFF")
        
    }


}
