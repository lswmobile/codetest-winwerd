//
//  Exhibits.swift
//  CodeTest
//
//  Created by Popeye on 6/2/2558 BE.
//  Copyright (c) 2558 lsw. All rights reserved.
//

import Foundation

class Exhibits: NSObject {
    var name: String = ""
    var photo: String = ""
    var curatorPhoto: String = ""
    var taggedText: TaggedText = TaggedText()
}