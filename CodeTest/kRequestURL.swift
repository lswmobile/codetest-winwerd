//
//  kRequestURL.swift
//  CodeTest
//
//  Created by Popeye on 6/2/2558 BE.
//  Copyright (c) 2558 lsw. All rights reserved.
//

import Foundation

struct kRequestURL {

    static let kBaseURL = "https://s3-us-west-1.amazonaws.com/musea-aws/json/"
    static let kDataKey = "data"

    static let kMuseums = kBaseURL + "data.json"
    static let kExhibits = kBaseURL + "exhibits.json"
}
