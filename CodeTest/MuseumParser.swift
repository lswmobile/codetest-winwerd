//
//  MuseumParser.swift
//  CodeTest
//
//  Created by Popeye on 6/2/2558 BE.
//  Copyright (c) 2558 lsw. All rights reserved.
//

import Foundation

class MuseumParser: NSObject {

    class func parseObject(object: AnyObject) -> Museum {

        var parseObject: Museum = Museum()

        // The idea borrowed from https://gist.github.com/mchambers/fb9da554898dae3e54f2
        // we start at 1 because we want to skip super attribute
        for var index = 1; index < reflect(parseObject).count; ++index {

            let key = reflect(parseObject)[index].0

            // it will assign the value only if it's not null
            if let value = object.objectForKey(key) as? String {
                parseObject.setValue(value, forKey: key)
            }
        }

        return parseObject
    }
}