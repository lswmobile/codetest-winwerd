//
//  InfoViewController.swift
//  CodeTest
//
//  Created by Popeye on 6/2/2558 BE.
//  Copyright (c) 2558 lsw. All rights reserved.
//

import UIKit
import SwiftHTTP
import TTTAttributedLabel
import ReactiveCocoa

class InfoViewController: UITableViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating {

    var request:HTTPTask?
    var museums: NSMutableArray?
    var filteredMuseums: NSMutableArray?

    var searchController: UISearchController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "curated"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: kUIConstant.kFont.kDefault, size: kUIConstant.kNavigationBar.fontSize)!]
        self.navigationController?.navigationBar.barTintColor = kUIConstant.kColors.navBar
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.view.backgroundColor = UIColor.lightGrayColor()

        // setup navbar buttons
        var searchButton: UIButton = UIButton()
        searchButton.setImage(UIImage(named: "search"), forState: UIControlState.Normal)
        searchButton.frame = kUIConstant.kNavigationBar.navbarButtonFrame
        searchButton.addTarget(self, action: "searchPerformed", forControlEvents: UIControlEvents.TouchUpInside)

        var menuButton: UIButton = UIButton()
        menuButton.setImage(UIImage(named: "bars"), forState: UIControlState.Normal)
        menuButton.frame = kUIConstant.kNavigationBar.navbarButtonFrame

        let rightItem: UIBarButtonItem = UIBarButtonItem()
        rightItem.customView = searchButton
        self.navigationItem.rightBarButtonItem = rightItem

        let leftItem: UIBarButtonItem = UIBarButtonItem()
        leftItem.customView = menuButton
        self.navigationItem.leftBarButtonItem = leftItem


        // Setup Search Display Controller
        self.searchController = UISearchController(searchResultsController: nil) // pass nil for showing the result in the same page
        self.searchController!.delegate = self
        self.searchController!.searchResultsUpdater = self
        self.searchController!.searchBar.delegate = self
        self.searchController!.dimsBackgroundDuringPresentation = false

        // Setup Search Bar
        self.searchController!.searchBar.frame = CGRectMake(0, 0, self.view.frame.width, kUIConstant.kSearchBar.height)
        self.searchController!.searchBar.delegate = self
        self.searchController!.searchBar.placeholder = "Search for Museum"
        self.searchController!.searchBar.searchBarStyle = UISearchBarStyle.Default
        self.searchController!.searchBar.translucent = false
        self.searchController!.searchBar.sizeToFit()

        // Attatch search to tableView
        self.tableView.tableHeaderView = self.searchController?.searchBar
        // Hide search bar at start
        self.tableView.setContentOffset(CGPointMake(0, 44), animated: true)
        self.view.backgroundColor = UIColor.whiteColor()

        // Initialise musium array
        self.museums = []
        self.filteredMuseums = []

        // Fetch meseums to the list
        self.requestMuseums()

        // Remove separator line
        self.tableView.separatorInset = UIEdgeInsetsZero
        self.tableView.layoutMargins = UIEdgeInsetsZero
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None

        // hide status bar when search
        self.definesPresentationContext = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Send request funtions
    func requestMuseums() {
        // Send GET request to populate the musium
        self.request = HTTPTask()
        //The expected response will be JSON and be converted to an object return by NSJSONSerialization instead of a NSData.
        self.request?.responseSerializer = JSONResponseSerializer()

        self.request?.GET(kRequestURL.kMuseums, parameters: nil, completionHandler: { (response: HTTPResponse) -> Void in
            println("Response from \(response.URL!) has arrived with status \(response.statusCode).")
            if let err = response.error {
                println("error: \(err.localizedDescription)")
                return // we may need to nitify app for failure; show alert for example
            }

            if let array:NSArray = response.responseObject!.objectForKey(kRequestURL.kDataKey) as? NSArray {
                println("Parsing Museum object...")
                array.bk_each({ (each: AnyObject!) -> Void in
                    let museum = MuseumParser.parseObject(each)
                    self.museums?.addObject(museum)

                    println("\t- \(museum.name)")
                })
                println("pairsed \(self.museums!.count) object(s)")

                // Refresh table view for displaying data
                dispatch_async(dispatch_get_main_queue()) { // doing this in main thread to make it update immediately
                    let transition = CATransition()
                    transition.type = kCATransitionPush
                    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                    transition.fillMode = kCAFillModeForwards
                    transition.duration = kUIConstant.kTable.kCell.animationDuration
                    transition.subtype = kCATransitionFromTop
                    self.tableView.layer.addAnimation(transition, forKey: "UITableViewReloadDataAnimationKey")
                    // Update your data source here
                    self.tableView.reloadData()
                }
            }
            else {
                println("There are no data sending back")
            }
        })
    }

    // MARK - UITableViewDataSource implementation
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.museums == nil {
            return 0
        }
        if self.searchController!.active {
            return self.filteredMuseums!.count
        }
        return self.museums!.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifierString = "InfoTableViewCell"

        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier(identifierString) as? UITableViewCell

        if cell == nil {

            cell = InfoTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: identifierString)
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
        }

        var museum: Museum?

        if self.searchController!.active {
            // filtered data
            museum = self.filteredMuseums?.objectAtIndex(indexPath.row) as? Museum
        }
        else {
            // all data
            museum = self.museums?.objectAtIndex(indexPath.row) as? Museum
        }

        // inject data to cell's UI
        var infoTableViewCell = (cell as! InfoTableViewCell)
        if museum != nil {
            infoTableViewCell.nameLabel.text = museum!.name
            infoTableViewCell.museumImage.sd_setImageWithURL(NSURL(string: museum!.photo))
            infoTableViewCell.locationLabel.text = museum!.location

            infoTableViewCell.applyFilter(indexPath.row)
        }

        cell?.layoutMargins = UIEdgeInsetsZero

        return cell!
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let exhibitsViewContoller = ExhibitsViewController()
        self.navigationController!.pushViewController(exhibitsViewContoller, animated: true)
    }

    // Custom height for all cells
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return kUIConstant.kTable.kCell.kMuseum.height
    }

    // MARK: - Search
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        println("current search text \(searchText)")

        self.search(searchText)
    }

    func search(searchText: String) {

        let predicate = NSPredicate { (obj: AnyObject!, dic: [NSObject : AnyObject]!) -> Bool in
            if searchText.isEmpty {
                return true;
            }
            let museum = (obj as! Museum)
            return museum.name.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil, locale: nil) != nil
        }
        // filter the museums list
        self.filteredMuseums = NSMutableArray(array: self.museums!.filteredArrayUsingPredicate(predicate))
        dispatch_async(dispatch_get_main_queue()) {
            let transition = CATransition()
            transition.type = kCATransitionFade
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.fillMode = kCAFillModeForwards
            transition.duration = kUIConstant.kTable.kCell.animationDuration
            transition.subtype = kCATransitionFromTop
            self.tableView.layer.addAnimation(transition, forKey: "UITableViewReloadDataAnimationKey")
            self.tableView.reloadData()
        }
    }

    // MARK: - UISearchResultsUpdating
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        self.filteredMuseums?.removeAllObjects()

        self.search(self.searchController!.searchBar.text)
    }

    func searchPerformed() {

        if self.tableView.contentOffset.y < 44 {
            self.tableView.setContentOffset(CGPointMake(0, 44), animated: true)
        }
        else {
            self.tableView.setContentOffset(CGPointMake(0, 0), animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
